(function()
{
    var getView = function(view){
        return 'src/entrevista/views/' + view + '.html';
    }

    var entrevistaModule = angular.module('exercito.entrevista',[]);

    entrevistaModule.config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider){


        $stateProvider.state('entrevista-pergunta', {
                url: '/entrevista/pergunta/:perguntaId',
                cache:false,
                templateUrl: getView('pergunta'),
                controller: 'entrevista.PerguntaCtrl'
            })

            .state('entrevista-list-blocos', {
                url: '/entrevista/edit/listBlocos',
                cache:false,
                templateUrl: getView('questionario.list.blocos'),
                controller: 'questionario.ListBlocos'
            })

            .state('entrevista-list-perguntas', {
                url: '/entrevista/edit/listPerguntas',
                cache:false,
                templateUrl: getView('questionario.list.perguntas'),
                controller: 'questionario.ListPerguntas'
            })

            .state('entrevista-comentarios', {
                url: '/entrevista/comentarios',
                cache:false,
                templateUrl: getView('comentarios'),
                controller: 'entrevista.ComentariosCtrl'
            })

            .state('entrevista-comentarios-form', {
                url: '/entrevista/comentario/form',
                cache:false,
                templateUrl: getView('comentarios.form'),
                controller: 'entrevista.ComentariosFormCtrl'
            })
            .state('entrevista-atuacoes', {
                url: '/entrevista/atuacoes',
                cache:false,
                templateUrl: getView('atuacoes'),
                controller: 'entrevista.AtuacoesCtrl'
            })
    }])
}());