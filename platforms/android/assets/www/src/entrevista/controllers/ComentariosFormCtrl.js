(function()
{
    var ComentariosFormCtrl = function($scope,$http, Entrevistado, $stateParams, Parametro, $state,DB)
    {

        $scope.entrevistado = Entrevistado.getEntrevistado();

        $scope.data = {};
        $scope.save = function(){

            $scope.data.entrevistado_id = $scope.entrevistado._id;
            $scope.data.tipo = 'O';
            $scope.data.dt_criacao = moment(new Date()).format('YYYY-MM-DD H:m:s');
            $scope.data.dt_lembrete = null;
            $scope.data.sync = null;
            Parametro.get('pesquisador',function(res){$scope.data.usuario_id = res;});
            Parametro.get('nome',function(res){$scope.data.nome = res;});

            DB.save('historico',$scope.data,function(res){
                $state.go('entrevista-comentarios');
            });
        }




        };
    angular.module('exercito.entrevista').controller('entrevista.ComentariosFormCtrl',['$scope','$http','entrevista.Entrevistado','$stateParams','common.Parametro','$state','data.DB', ComentariosFormCtrl]);
}());
