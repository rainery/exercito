(function()
{
    var ComentariosCtrl = function($scope,$http, Entrevistado, $stateParams, Parametro, $state,DB)
    {

        $scope.entrevistado = Entrevistado.getEntrevistado();
        $scope.historico = {};
        $scope.usuario = {};
        Parametro.get('avatar',function(avatar){$scope.usuario.avatar = avatar;});
        Parametro.get('pesquisador',function(pesquisador){$scope.usuario.pesquisador = pesquisador;});


        var sqlHistorico =  "SELECT * FROM 'historico' h WHERE h.entrevistado_id = '"+$scope.entrevistado._id+"' ORDER BY h.dt_criacao DESC";
        DB.query(sqlHistorico, function(res){
            $scope.historico = res;
        });

        $scope.returnPerfil = function () {
           $state.go('entrevista-perfil',{entrevistadoId:$scope.entrevistado.ID});
        }

    };
    angular.module('exercito.entrevista').controller('entrevista.ComentariosCtrl',['$scope','$http','entrevista.Entrevistado','$stateParams','common.Parametro','$state','data.DB', ComentariosCtrl]);
}());
