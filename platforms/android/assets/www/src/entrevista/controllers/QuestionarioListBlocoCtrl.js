(function()
{
    var QuestionarioEdit = function($scope, $state, Entrevistado, DB)
    {

        /*
        Retorna para o perfil do dashboard
        */
        $scope.returnPerfil = function () {
            $state.go('dashboard.realizadas');
            Entrevistado.setEntrevista({});
        };


        var sqlBloco = "SELECT * FROM 'bloco' b ORDER BY b.ordem";
        DB.query(sqlBloco,function(blocos){

            var totalBlocos = blocos.length;
            var b = 0;
            $scope.blocos = blocos;

            angular.forEach($scope.blocos, function(bloco){
                b++;
                var sqlPergunta = "SELECT * FROM 'pergunta' p WHERE p.bloco_id = '"+bloco.id+"' ORDER BY p.ordem";
                DB.query(sqlPergunta,function(perguntas){

                    bloco.perguntas = perguntas;
                    var totalPerguntas = perguntas.length;
                    var p = 0;

                    // Foreach no objeto de perguntas para cada pergunta dentro dele pegar as opçoes
                    angular.forEach(perguntas, function(pergunta){
                        var sqlOpcao = "SELECT o.*, l.pergunta_id as logica_pergunta_id, l.bloco_id FROM 'opcao' o LEFT JOIN 'logica' l ON l.opcao_id = o.id WHERE o.pergunta_id = '"+pergunta.id+"'";
                        DB.query(sqlOpcao,function(opcoes){
                            p++;
                            pergunta.opcoes = opcoes;

                            if (p == totalPerguntas && b == totalBlocos) {
                                Entrevistado.setBlocoEntrevistado(blocos);
                                if(b == 1){
                                    $scope.goListPergunta(blocos[0]);
                                }
                            }
                        });
                    });
                });
            });

        });




        /*
        Retorna o usuário para a listagem de perguntas
        */
        $scope.goListPergunta = function (bloco) {
            Entrevistado.setPerguntasEntrevistado(bloco.perguntas);
            $state.go('entrevista-list-perguntas',{});
        };

    };
    angular.module('exercito.entrevista').controller('questionario.ListBlocos',['$scope','$state','dashboard.Entrevistado','data.DB', QuestionarioEdit]);
}());
