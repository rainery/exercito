(function()
{
    var PerguntaCtrl = function($scope,$http, Entrevistado, $stateParams, Parametro, $state,DB,$ionicModal, $ionicPopup, $timeout,$ionicLoading)
    {



        $ionicLoading.show({
            template: 'Carregando...'
        });


        var perguntas = [
            97,14,100,98,101,99,15,16,17,
            18,19,20,21,23,24,
            26,27,37,30,41,43,44,46,29,47,49,
            50,51,53,54,55,56,57,58,59,60,
            61,62,63,64,65,66,67,68,69,70,
            71,72,73,74,75,76,77,78,79,80,
            81,82,83,84,85,86,87,88,89,90,91,92,93
        ];

        angular.forEach(perguntas, function(index){
            if($stateParams.perguntaId == index){
                $timeout(function(){
                    $ionicLoading.hide();
                    if ($stateParams.perguntaId == 54) {
                        $scope.alerta();
                    }
                },500);
            } else {
                $timeout(function(){
                    $ionicLoading.hide();
                },3500);
            }
        });

        $scope.entrevista = {};
        $scope.endereco = false;
        $scope.perguntaId = $stateParams.perguntaId;
        $scope.entrevista = Entrevistado.getEntrevista();
        $scope.perguntas = {};
        $scope.pergunta = {};
        $scope.check = true;
        $scope.blocos = Entrevistado.getBlocoEntrevistado();
        $scope.retorno = Entrevistado.getRetorno();
        Entrevistado.setPrioridade();

        $scope.resposta = {
            resposta: '',
            opcao_id: null,
            entrevista_id: null,
            pergunta_id: null
        };

        $scope.ultimaPergunta = 0;
        $scope.nextBloco = 0;
        var b = 0;
        var p = 0;
        Entrevistado.setPrioridade();


        if ($stateParams.perguntaId == 99 ||  $stateParams.perguntaId == 101) {
            $scope.endereco = true;
        }

        if ($stateParams.perguntaId == 96) {
            var sqlResposta = "SELECT * FROM 'resposta' r WHERE r.entrevista_id = '"+$scope.entrevista.ID+"' AND r.pergunta_id = '47'";
            DB.query(sqlResposta, function(resposta){
                if(resposta[0].opcao_id == 194){
                    $state.go('entrevista-pergunta',{perguntaId:49});
                }
            });
        }

        if ($stateParams.perguntaId == 51) {
            $scope.$watch("pergunta.opcoes[5].checked", function(novo, antigo){
                if (novo == true) {
                    for (var i = 0; i <= 4; i++) {
                        $scope.pergunta.opcoes[i].checked = false;
                    }
                    $scope.checkFalse = true;
                } else {
                    $scope.checkFalse = false;
                }
            });
        }

        if ($stateParams.perguntaId == 31) {
            $scope.$watch("pergunta.opcoes[3].checked", function(novo, antigo){
                if (novo == true) {
                    console.log("AQUU");
                    for (var i = 0; i <= 2; i++) {
                        $scope.pergunta.opcoes[i].checked = false;
                    }
                    $scope.checkFalse = true;
                } else {
                    $scope.checkFalse = false;
                }
            });
        }

            $scope.alerta =  function(){
                var alertPopup = $ionicPopup.alert({
                    title: 'ATENÇÃO!',
                    template: 'Daqui para frente você vai responder só se CONCORDA TOTALMENTE, CONCORDA EM PARTE, NÃO CONCORDA NEM DISCORDA, DISCORDA EM PARTE OU DISCORDA TOTALMENTE.'
                });
                alertPopup.then(function(res) {
                    console.log('Alerta Dado');
                });
            };

        $scope.checkFalse = false;
        $timeout(function(){
            if ($stateParams.perguntaId == 23) {
                $scope.$watch("pergunta.opcoes[10].checked", function(novo, antigo){
                    if (novo == true) {
                        for (var i = 0; i <= 9; i++) {
                            $scope.pergunta.opcoes[i].checked = false;
                            $scope.pergunta.opcoes[i].prioridade = "TESTE";
                            $scope.pergunta.opcoes[i].resposta = "";
                        }
                        Entrevistado.setPrioridade(1);
                        $scope.pergunta.opcoes[10].prioridade = 1;
                        $scope.checkFalse = true;
                        $scope.verificaCheck(10,true);
                    } else {
                        $scope.verificaCheck(10,false);
                        $scope.checkFalse = false;
                    }
                });
            }
        }, 300);

        $scope.verificaCheck = function($index, check) {


            $scope.check = true;
            angular.forEach($scope.pergunta.opcoes, function (op) {
                if (op.checked == true) {
                    $scope.check = false;
                }
            });

            if($index != 10 || $stateParams.perguntaId != 23) {

                    if($scope.pergunta.prioridade == 1){
                             Entrevistado.getPrioridade(function(p){
                                 if (p == undefined) {
                                     p = 0;
                                 }
                                 if (check && p < 5) {
                                     p++;
                                     $scope.pergunta.opcoes[$index].prioridade = p;
                                     Entrevistado.setPrioridade(p);
                                 } else if (check && p == 5) {
                                     alert("NO MÁXIMO 5 OPCÕES");
                                     $scope.pergunta.opcoes[$index].checked = false;
                                 } else if (!check) {
                                     p = p - 1;
                                     angular.forEach($scope.pergunta.opcoes, function (op) {
                                         if (op.checked == true) {
                                             var i = 1;
                                             angular.forEach($scope.pergunta.opcoes, function (op2) {
                                                 if (op2.checked == true && op2.id != op.id) {
                                                     if (op.prioridade > op2.prioridade) {
                                                         i++;
                                                     }
                                                 }
                                             });
                                             op.prioridade = i;
                                         }
                                     });
                                     Entrevistado.setPrioridade(p);
                                 }
                             });
                    }
            }
        };


        /*
        Procura onde está a pergunta da URL
         */
        angular.forEach ($scope.blocos, function(bloco) {
            p = 0 ;
            b++;
            angular.forEach (bloco.perguntas, function(pergunta) {
                p++;
                if (pergunta.id == $stateParams.perguntaId) {
                    $scope.nome_bloco = bloco.bloco;
                    if(p == 1 && b == 1){
                        $scope.disabledVoltar = 1;
                    }
                    $scope.pergunta = angular.copy(pergunta);
                    $scope.resposta.pergunta_id = angular.copy(pergunta.id);
                    $scope.progressBloco = b;
                    $scope.progress = p;
                    if (b == $scope.blocos.length && p == bloco.perguntas.length) {
                        $scope.ultimaPergunta = 1;
                    }
                    if (p == bloco.perguntas.length && $scope.blocos.length > $scope.progressBloco) {
                       $scope.nextBloco = 1;
                    }
                    $scope.totalPerguntas = $scope.blocos[b-1].perguntas.length;
                    $scope.progressBar = $scope.progress*100/$scope.blocos[b-1].perguntas.length;
                }
            });
        });
        $scope.totalBlocos = b;

        /*
         Verifica se o usuário vai iniciar ou editar um pergunta
         */
        if ($scope.entrevista != null) {

                if($scope.entrevista.id != null && $scope.entrevista.id != undefined){
                    $scope.entrevista.ID = $scope.entrevista.id;
                }
                var sqlResposta = "SELECT * FROM 'resposta' r WHERE r.entrevista_id = '"+$scope.entrevista.ID+"' AND r.pergunta_id = "+$stateParams.perguntaId+"";
                DB.query(sqlResposta, function(resposta){
                    if(resposta.length != 0) {
                        if($scope.pergunta.tipo == "C"){
                            $scope.check = true;
                            var prioridade = 0;
                            angular.forEach($scope.pergunta.opcoes, function(opcao){
                                angular.forEach(resposta, function(res){
                                    if (opcao.id == res.opcao_id) {
                                        opcao.checked = true;
                                        opcao.prioridade = res.prioridade;
                                        opcao.resposta_id = res.id;
                                        if(opcao.texto  == 1){
                                            opcao.resposta = res.resposta;
                                        }
                                        $scope.check = false;
                                        prioridade++;
                                        Entrevistado.setPrioridade(prioridade);
                                    }
                                });
                            });
                        } else {
                            $scope.resposta = angular.copy(resposta[0]);
                            if ($scope.pergunta.tipo == 'N') {
                                $scope.resposta.resposta = parseInt(resposta[0].resposta);
                            }
                        }


                        var sqlUltima = 'SELECT r.* FROM resposta r WHERE entrevista_id = '+$scope.entrevista.ID+' AND pergunta_id = '+$stateParams.perguntaId;
                        DB.query(sqlUltima, function(ultima) {
                            if(ultima.length != 0) {
                                Entrevistado.setUltimaPergunta(ultima[0].ultima_pergunta);
                            }
                        });

                        $scope.$apply();
                    } else {
                        $scope.$apply();
                    }
                });

        } else {
            $ionicLoading.hide();
            $scope.check = true;
        }

        /*
         Volta para pergunta anterior caso ela exista
         */
        $scope.returnPergunta = function(){
            var ultima = Entrevistado.getUltimaPergunta();
            var sqlResposta = 'SELECT r.* FROM resposta r WHERE entrevista_id = '+$scope.entrevista.ID+' AND pergunta_id = '+$stateParams.perguntaId;
            DB.query(sqlResposta, function(resposta) {
                if(resposta.length != 0 && ultima == undefined){
                    Entrevistado.setUltimaPergunta();
                    $state.go('entrevista-pergunta',{perguntaId:resposta[0].ultima_pergunta});
                } else {
                    Entrevistado.setUltimaPergunta();
                    $state.go('entrevista-pergunta',{perguntaId:ultima});
                }
            });
        };
        $scope.status = Entrevistado.getStatusEntrevista();



        $scope.excluirEntrevista =  function(){
            DB.remove("entrevista", $scope.entrevista.ID, function () {
                var sqlResposta = "SELECT * FROM 'resposta' r WHERE r.entrevista_id = '" + $scope.entrevista.ID + "'";
                DB.query(sqlResposta, function (respostas) {
                    var r = respostas.length;
                    if (r > 0) {
                        var i = 0;
                        angular.forEach(respostas, function (resposta) {
                            DB.remove("resposta", resposta.id, function () {
                                i++;
                                if (i == r) {
                                    Entrevistado.setPrioridade(null);
                                    if ($scope.status != 1) {
                                        $state.go('dashboard.resumo');
                                    } else {
                                        $state.go('dashboard.realizadas');
                                    }
                                }
                            });
                        });
                    } else {
                        Entrevistado.setPrioridade(null);
                        if ($scope.status != 1) {
                            $state.go('dashboard.resumo');
                        } else {
                            $state.go('dashboard.realizadas');
                        }
                    }

                });
            });
        };

        $scope.edit = Entrevistado.getEdit();
        /*
        Se entiver na edição volta para a listagem de perguntas, caso contrário para o perfil
         */
        $scope.returnPerfil = function () {
            if($scope.edit == 0) {
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Alerta',
                    template: 'Tem certeza que deseja sair, você perdera todos dos dados desta entrevista?',
                    cancelText: 'CANCELAR',
                    cancelType: '',
                    okText: 'SAIR',
                    okType: ''
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        if ($scope.entrevista != undefined) {
                            $scope.excluirEntrevista();
                        } else {
                            Entrevistado.setPrioridade(null);
                            if ($scope.status != 1) {
                                $state.go('dashboard.resumo');
                            } else {
                                $state.go('dashboard.realizadas');
                            }
                        }
                    } else {
                        console.log('You are not sure');
                    }
                });
            } else {
                $state.go("dashboard.realizadas");
            }
        };

        /*
         Envia o usuário para a próxima pergunta
         Salva a entrevista caso ela não foi iniciada
         Salva a resposta do usuário
         Verifica se essa foi a última pergunta do questionário
        */
        $scope.save = function(){

            if ($scope.nextBloco == 1){
                $scope.progressBloco++;
                $scope.progress = 0;
            }

            Entrevistado.setPrioridade(null);

            $scope.validResposta($scope.resposta, function(res){
               if(res == true){
                   if ($scope.entrevista != null) {
                       $scope.resposta.entrevista_id = $scope.entrevista.ID;
                       $scope.saveResposta($scope.resposta);
                   } else if($scope.entrevista == null) {
                       $scope.iniciaEntrevista();
                   }
               } else {
                   $scope.excluirEntrevista();
               }
           });
        };


        /*
        Salva a resposta no banco de dados local
         */
        $scope.validResposta = function(resposta, callback) {

            var sqlCota = 'SELECT c.* FROM cota c WHERE c.pergunta_id = '+$stateParams.perguntaId;
            DB.query(sqlCota, function(cota){

                if(cota.length == 0){
                    callback(true);
                } else {
                    if(resposta.opcao_id != null){
                        var sqlFaixas = 'SELECT f.* FROM faixa f WHERE f.opcao_id = '+resposta.opcao_id+'';
                        DB.query(sqlFaixas, function(faixa){
                            var sqlRespostas = 'SELECT COUNT(*) as total FROM resposta r WHERE r.opcao_id = '+resposta.opcao_id;
                            DB.query(sqlRespostas, function(respostas){

                                Parametro.get('municipio',function(municipio){
                                    var sqlFrequencia = 'SELECT f.* FROM frequencia f WHERE f.faixa_id = '+faixa[0].id+
                                        ' AND f.municipio_id = '+municipio+'';
                                    DB.query(sqlFrequencia, function(frequencia){
                                        var f = frequencia[0].value;
                                        var r = respostas[0].total;
                                        if(f > r){
                                            callback(true);
                                        } else {
                                            alert("ESSA COTA JA FOI ATINGIDA!");
                                            callback(false);
                                        }
                                    });
                                });


                            });
                        });
                    } else {

                        var sqlFaixas = 'SELECT f.* FROM faixa f WHERE cotas_id = '+cota[0].id+' AND f.minimo <= '+parseInt(resposta.resposta)+' and f.maximo >= '+parseInt(resposta.resposta)+'';

                        DB.query(sqlFaixas, function(faixa){

                            var sqlRespostas = 'SELECT COUNT(*) as total FROM resposta r WHERE r.resposta >= '+faixa[0].minimo+' AND r.resposta <= '+faixa[0].maximo+' AND r.pergunta_id = '+$stateParams.perguntaId+'';
                            DB.query(sqlRespostas, function(respostas){
                                Parametro.get('municipio',function(municipio){
                                    var sqlFrequencia = 'SELECT f.* FROM frequencia f WHERE f.faixa_id = '+faixa[0].id+
                                        ' AND f.municipio_id = '+municipio+'';
                                    DB.query(sqlFrequencia, function(frequencia){
                                        var f = frequencia[0].value;
                                        var r = respostas[0].total;

                                        if(f > r){
                                            callback(true);
                                        } else {
                                            alert("ESSA COTA JA FOI ATINGIDA");
                                            callback(false);
                                        }
                                    });
                                });
                            });
                        });
                    }
                }

            });
        };

        /*
        Salva a resposta no banco de dados local
         */
        $scope.saveResposta = function(resposta) {


            if($scope.pergunta.tipo != "C") {
                delete(resposta.check);

                resposta.ultima_pergunta = Entrevistado.getUltimaPergunta();
                DB.save('resposta', resposta, function (res) {
                    if ($scope.ultimaPergunta == 0) {
                        $scope.nextPergunta();

                    } else {
                        $scope.finalizaEntrevista();
                    }
                });
            } else {
                console.log("DENTRO DO SAVE DE CHECKBOX");
                $scope.i = 1;
                $scope.ultima = Entrevistado.getUltimaPergunta();
                angular.forEach ($scope.pergunta.opcoes, function(opcao) {

                    if (opcao.checked == true) {
                        var resp = {};
                        console.log(angular.toJson(opcao));
                        resp.pergunta_id = resposta.pergunta_id;
                        resp.entrevista_id = resposta.entrevista_id;
                        resp.opcao_id = opcao.id;
                        resp.prioridade = opcao.prioridade;
                        resp.ultima_pergunta = $scope.ultima;

                        if(opcao.resposta_id != undefined){
                            resp.id = opcao.resposta_id;
                        }
                        if(opcao.texto == 1){
                            resp.resposta = opcao.resposta;
                        }
                        DB.save('resposta', resp, function (res) {
                             if($scope.i == $scope.pergunta.opcoes.length){
                                 if ($scope.ultimaPergunta == 0) {
                                     $scope.nextPergunta();
                                 } else {
                                     $scope.finalizaEntrevista();
                                 }
                             } else {
                                 $scope.i = $scope.i+1;
                             }
                        });
                    } else {
                        var sqlResposta = 'SELECT r.* FROM resposta r WHERE entrevista_id = '+resposta.entrevista_id+' AND opcao_id = '+opcao.id;
                        DB.query(sqlResposta, function(resposta){
                            if(resposta.length != 0) {
                                DB.remove("resposta", resposta[0].id, function () {
                                    if ($scope.i == $scope.pergunta.opcoes.length) {
                                        if ($scope.ultimaPergunta == 0) {
                                            $scope.nextPergunta();
                                        } else {
                                            $scope.finalizaEntrevista();
                                        }
                                    } else {
                                        $scope.i = $scope.i+1;
                                    }
                                })
                            } else if (resposta.length == 0) {
                                if ($scope.i == $scope.pergunta.opcoes.length) {
                                    if ($scope.ultimaPergunta == 0) {
                                        $scope.nextPergunta();
                                    } else {
                                        $scope.finalizaEntrevista();
                                    }
                                } else {
                                    $scope.i = $scope.i+1;
                                }
                            }
                        });
                    }
                });
            }

        };

        /*
        Verifica qual será a próxima pergunta a ser respondida
         */
        $scope.nextPergunta = function(){

            Entrevistado.setUltimaPergunta($stateParams.perguntaId);
            if ($scope.pergunta.tipo == 'R' && $scope.pergunta.opcoes[0].logica_pergunta_id ) {
                for (var i = 0; i < $scope.pergunta.opcoes.length; i++) {
                    if ($scope.pergunta.opcoes[i].id == $scope.resposta.opcao_id) {
                        $state.go('entrevista-pergunta', {perguntaId: $scope.pergunta.opcoes[i].logica_pergunta_id});
                    }
                }
            } else {
                    if($scope.checkFalse == true && $stateParams.perguntaId != 51 && $stateParams.perguntaId != 31){
                        if ($stateParams.perguntaId == 23) {
                            Entrevistado.setUltimaPergunta(23);
                            $state.go('entrevista-pergunta', {perguntaId: 26});
                        } else if ($stateParams.perguntaId == 22) {

                        }
                    } else {
                        $state.go('entrevista-pergunta', {perguntaId: $scope.blocos[$scope.progressBloco - 1].perguntas[$scope.progress].id});
                    }
            }
            $scope.$apply();

        };

        /*
        Função responsável por finalizar a entrevista salvando o termino
         */
        $scope.finalizaEntrevista = function() {

            var sqlEntrevista = 'SELECT e.* FROM entrevista e WHERE e.id = '+$scope.entrevista.ID;
            DB.query(sqlEntrevista, function(entrevista){

                $scope.entrevista = angular.copy(entrevista[0]);
                $scope.entrevista.termino = moment(new Date()).format('YYYY-MM-DD H:m:s');
                DB.save('entrevista',$scope.entrevista,function(res){
                   Entrevistado.setEntrevista(null);
                  var entrevista  =  Entrevistado.getStatusEntrevista();
                    if (entrevista != 1) {
                        $state.go('dashboard.resumo');
                    } else {
                        $state.go('dashboard.realizadas');
                    }
                    $scope.$apply();
                });
            });

        };

        var i = 0;
        $scope.$watch("resposta.opcao_id",function(novo, antigo){
            if(novo != undefined && novo != null && i > 1){
                $scope.resposta.resposta = "";
            } else {
                i++;
            }
        });

        $ionicModal.fromTemplateUrl('./src/entrevista/views/modal.texto.option.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modalOption = modal;
        });

        $ionicModal.fromTemplateUrl('./src/entrevista/views/modal.texto.check.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modalCheck = modal;
        });


        $scope.openModal = function(pergunta, opcao) {
            $scope.pergunta = pergunta;
            $scope.opcao = opcao;
            $scope.modalOption.show();
        };
        $scope.closeModal = function(data) {
            $scope.resposta.resposta = "";
            $scope.modalOption.hide();
        };
        $scope.saveModal = function() {
            $scope.modalOption.hide();
        };

        $scope.openModalCheck = function(pergunta, opcao,i, check) {
            if ($scope.checkFalse != true) {

                $scope.pergunta = pergunta;
                $scope.opcao = opcao;
                $scope.index = i;
                if(check){
                    Entrevistado.getPrioridade(function(p){
                        if (p < 5 || $scope.pergunta.prioridade == 0) {
                            $scope.modalCheck.show();
                        } else {
                            alert("No MÁXIMO 5 OPCOES");
                            $scope.pergunta.opcoes[i].checked = false;
                        }
                    });
                } else {
                    $scope.modalCheck.show();
                }
            }
        };

        $scope.closeModalCheck = function(i) {
            $scope.pergunta.opcoes[i].checked = false;
            $scope.pergunta.opcoes[i].prioridade = "vazio";
            $scope.pergunta.opcoes[i].resposta = "";
            $scope.modalCheck.hide();
            $scope.verificaCheck(i,false);
        };

        $scope.saveModalCheck = function(i) {
            $scope.check = false;
            $scope.pergunta.opcoes[i].checked = true;
            console.log($scope.pergunta.opcoes[i].prioridade);
            if(isNaN($scope.pergunta.opcoes[i].prioridade)){
                Entrevistado.getPrioridade(function(a){
                    $scope.pergunta.opcoes[i].prioridade = a+1;
                    Entrevistado.setPrioridade(a+1);
                    $scope.modalCheck.hide();
                });
            } else {
                $scope.modalCheck.hide();
            }

        };





        //Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
            $scope.modalOption.remove();
        });
        // Execute action on hide modal
        $scope.$on('modalOption.hidden', function() {
            // Execute action
        });
        // Execute action on remove modal
        $scope.$on('modalOption.removed', function() {
            // Execute action
        });



        //Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
            $scope.modalCheck.remove();
        });
        // Execute action on hide modal
        $scope.$on('modalCheck.hidden', function() {
            // Execute action
        });
        // Execute action on remove modal
        $scope.$on('modalCheck.removed', function() {
            // Execute action
        });





        /*
        Salva no banco e inicia a entrevista
         */
        $scope.iniciaEntrevista = function(){

            $scope.entrevista = {};
            Parametro.get('pesquisador',function(res){$scope.entrevista.usuario_id = res;});
            $scope.entrevista.dt_entrevista = moment(new Date()).format('YYYY-MM-DD');
            $scope.entrevista.inicio = moment(new Date()).format('YYYY-MM-DD H:m:s');
            $scope.entrevista.latlong = '000000';

            DB.save('entrevista',$scope.entrevista,function(){
                var sqlEntrevistas =  "SELECT MAX(e.id) as ID, e.inicio FROM 'entrevista' e";
                DB.query(sqlEntrevistas,function(res){

                    Entrevistado.setEntrevista(res[0]);
                    $scope.resposta.entrevista_id = res[0]['ID'];
                    $scope.saveResposta($scope.resposta);

                });
            });
        }

    };
    angular.module('exercito.entrevista').controller('entrevista.PerguntaCtrl',['$scope','$http','dashboard.Entrevistado','$stateParams','common.Parametro','$state','data.DB','$ionicModal','$ionicPopup','$timeout', '$ionicLoading', PerguntaCtrl]);
}());
