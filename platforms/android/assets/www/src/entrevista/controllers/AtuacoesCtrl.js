(function()
{
    var AtuacoesCtrl = function($scope,$state, $stateParams, DB, Entrevistado, Parametro)
    {
        $scope.entrevistado = {};
        $scope.entrevistado = Entrevistado.getEntrevistado();
        $scope.atuacoes = {};
        $scope.invalid = true;
        $scope.area_atuacao = {};
        $scope.area_atuacao.entrevistado__id = $scope.entrevistado._id ;
        $scope.area_atuacao.atuacao_id = '';

        var sqlAtuacoes = "SELECT a.* FROM 'atuacao' a WHERE a.id NOT IN " +
            "(SELECT at.atuacao_id FROM 'area_atuacao' at WHERE at.entrevistado__id = "+$scope.entrevistado._id+") ";

        DB.query(sqlAtuacoes, function(res){
          $scope.atuacoes = res;
        });

        $scope.setAtuacao =  function(data){
            $scope.invalid = false;
        }

        $scope.saveAtuacao = function(){

            DB.save('area_atuacao',$scope.area_atuacao,function(res){
               var sqlAreaAtuacao =  "SELECT  MAX(at.id) as ID, a.* FROM 'area_atuacao' at " +
                   "LEFT JOIN 'atuacao' a ON a.id = at.atuacao_id";

                DB.query(sqlAreaAtuacao,function(res){

                   var len = $scope.entrevistado.atuacao.length;
                   $scope.entrevistado.atuacao[len] = {};
                   $scope.entrevistado.atuacao[len].inicio = null;
                   $scope.entrevistado.atuacao[len].termino = null;
                   $scope.entrevistado.atuacao[len].atuacao = res[0].atuacao;
                   $scope.entrevistado.atuacao[len].atuacao_id = res[0].id;
                   $scope.entrevistado.atuacao[len].entrevista_id = null;
                   $scope.entrevistado.atuacao[len].resposta_sync = null;
                   $scope.entrevistado.atuacao[len].entrevistado_id = $scope.entrevistado._id;
                   $scope.entrevistado.atuacao[len].id = res[0].ID;

                  $scope.goEntrevista($scope.entrevistado.atuacao[len]);
               })

            })
        }

        $scope.goEntrevista = function(atuacao){
            //Seta a atuação atual a respoder o questionario com intuito de salvar a entrevista com uma atuação_id
            Entrevistado.setAtuacao(atuacao);
            console.log(atuacao);
            var sqlQuestionario = "SELECT * FROM 'questionario' q WHERE q.perfil = '"+$scope.entrevistado.PERFIL+"'";
            DB.query(sqlQuestionario,function(questionario){


                if(questionario.length > 0){
                    var sqlBloco = "SELECT * FROM 'bloco' b WHERE b.questionario_id = '"+questionario[0].id+"'";
                    Entrevistado.setQuestionario(questionario[0].id);
                    DB.query(sqlBloco,function(blocos){
                        var totalBlocos = blocos.length, b = 0;
                        $scope.blocos = blocos;

                        angular.forEach($scope.blocos, function(bloco){
                            b++;
                            var sqlPergunta = "SELECT * FROM 'pergunta' p WHERE p.bloco_id = '"+bloco.id+"'";
                            DB.query(sqlPergunta,function(perguntas){
                                bloco.perguntas = perguntas;
                                var totalPerguntas = perguntas.length, p = 0;

                                // Foreach no objeto de perguntas para cada pergunta dentro dele pegar as opçoes
                                angular.forEach(perguntas, function(pergunta){
                                    var sqlOpcao = "SELECT o.*, l.pergunta_id as logica_pergunta_id, l.bloco_id FROM 'opcao' o LEFT JOIN 'logica' l ON l.opcao_id = o.id WHERE o.pergunta_id = '"+pergunta.id+"'";
                                    DB.query(sqlOpcao,function(opcoes){
                                        p++;
                                        pergunta.opcoes = opcoes;

                                        if (p == totalPerguntas && b == totalBlocos) {

                                            Entrevistado.setBlocoEntrevistado(blocos);
                                            if (atuacao.inicio == null) {
                                                $state.go('entrevista-pergunta',{perguntaId: $scope.blocos[0].perguntas[0].id})
                                            } else if (atuacao.inicio != null && atuacao.termino == null) {
                                                Parametro.get(atuacao.id,function (res) {
                                                    $state.go('entrevista-pergunta',{perguntaId: res})
                                                });
                                            } else {
                                                if($scope.blocos.lenght > 1){
                                                    $state.go('entrevista-list-blocos',{});
                                                } else {
                                                    Entrevistado.setPerguntasEntrevistado($scope.blocos[0].perguntas);
                                                    $state.go('entrevista-list-perguntas',{});
                                                }
                                            }
                                        }
                                    });
                                });
                            });
                        });

                    });
                } else {
                    alert("Esse perfil não tem questionário!");
                }
            });
        }


        /*
         Retorna ao perfil do dashboard
         */
        $scope.returnPerfil = function () {
                $state.go('entrevista-perfil',{entrevistadoId:$scope.entrevistado.ID});
        }



    };
    angular.module('exercito.entrevista').controller('entrevista.AtuacoesCtrl',['$scope','$state','$stateParams','data.DB','entrevista.Entrevistado','common.Parametro', AtuacoesCtrl]);
}());
