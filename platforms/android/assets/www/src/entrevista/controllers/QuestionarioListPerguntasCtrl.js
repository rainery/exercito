(function()
{
    var QuestionarioListPerguntas = function($scope, $state, Entrevistado)
    {

        $scope.perguntas = Entrevistado.getPerguntasEntrevistado();
        $scope.blocos = Entrevistado.getBlocoEntrevistado();

        /*
        Retorna para página anterior caso a entrevista seja composta por um único bloco ele volta diretamente para o perfil
        */
        $scope.returnBlocos = function () {
            if ($scope.blocos.length == 1) {
                $state.go('dashboard.realizadas');
            } else {
                $state.go('entrevista-list-blocos');
            }
        };

        /*
         Vai para edição da pergunta selecionada
        */
        $scope.goEditPergunta = function (pergunta) {
            $state.go('entrevista-pergunta',{perguntaId:pergunta.id});
        };

    };
    angular.module('exercito.entrevista').controller('questionario.ListPerguntas',['$scope','$state','dashboard.Entrevistado', QuestionarioListPerguntas]);
}());
