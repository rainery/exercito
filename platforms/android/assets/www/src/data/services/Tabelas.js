(function()
{
    var Tabelas = function()
    {

        return {

            createTables : function(db){
                    this.createTableParametros(db);
                    this.createTableQuestionario(db);
                    this.createTableBloco(db);
                    this.createTablePergunta(db);
                    this.createTableOpcao(db);
                    this.createTableLogica(db);
                    this.createTableEntrevista(db);
                    this.createTableResposta(db);
                    this.createTableFrequencia(db);
                    this.createTableCota(db);
                    this.createTableFaixa(db);
            } ,

            createTableParametros : function(db){
                var sql = "CREATE TABLE IF NOT EXISTS 'parametro' (" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "'chave' varchar(100) NULL," +
                    "'valor' varchar(100) NULL)";

                db.transaction(function(tx) {
                        tx.executeSql(sql, null,
                            function () {
                                console.log('Create table conta success');
                            },
                            function (tx, error) {
                                console.log('Create table error: ' + error.message);
                            });

                });

            },

            createTableEntrevistado : function(db){
                var sql = "CREATE TABLE IF NOT EXISTS 'dashboard' (" +
                    "'_id' INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "'ID' INTEGER NULL," +
                    "'RAZAOSOCIAL' varchar(255) NULL," +
                    "'pesquisa_id' INTEGER,"+
                    "'PERFIL' varchar(50),"+
                    "'ENDERECO' varchar(255),"+
                    "'BAIRRO' varchar(100),"+
                    "'MUNICIPIO' varchar(100),"+
                    "'UF' varchar(2),"+
                    "'TELEFONE' varchar(60),"+
                    "'PORTO' varchar(100),"+
                    "'UFPORTO' varchar(2),"+
                    "'TIPONAVEGACAO' varchar(45),"+
                    "'NOMECONTATO' varchar(100),"+
                    "'ANOTACAO' LONG TEXT,"+
                    "'tag_agendamento_cor' varchar(7),"+
                    "'tag_agendamento_label' varchar(30),"+
                    "'situacao_id' INTEGER NULL,"+
                    "'atuacao_tipo' varchar(4),"+
                    "'perfil_id' INTEGER)";

               /// sql = "DROP TABLE 'dashboard'";

                db.transaction(function(tx) {
                    tx.executeSql(sql, null,
                        function () {
                            console.log('Create table dashboard success');
                        },
                        function (tx, error) {
                            console.log('Create table error: ' + error.message);
                        });

                });

            },

            createTableQuestionario: function(db){

                var sql = "CREATE TABLE IF NOT EXISTS 'questionario' (" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    "'titulo' varchar(100) ," +
                    "'perfil' varchar(100) ," +
                    "'perfil_id' INTEGER ," +
                    "'dt_criacao' DATE TIME)";

               // sql = 'DROP TABLE "questionario"';

               db.transaction(function(tx){
                   tx.executeSql(sql,null,
                   function(){
                       console.log('Create table questionario success');
                   },
                   function(tx, error){
                       console.log('Create table error: ' + error.message);
                   });
               });
            },

            createTableBloco: function(db){

                var sql = "CREATE TABLE IF NOT EXISTS 'bloco' (" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    "'questionario_id' INTEGER , "+
                    "'ordem' INTEGER , "+
                    "'bloco' varchar(100) )" ;

                //sql = 'DROP TABLE "bloco"';

                db.transaction(function(tx){
                    tx.executeSql(sql,null,
                        function(){
                            console.log('Create table bloco success');
                        },
                        function(tx, error){
                            console.log('Create table error: ' + error.message);
                        });
                });
            },

            createTablePergunta: function(db){

                var sql = "CREATE TABLE IF NOT EXISTS 'pergunta' (" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    "'pergunta' varchar(100) ," +
                    "'tipo' varchar(2) ," +
                    "'obrigatoria' varchar(2) ," +
                    "'prioridade' INTEGER ," +
                    "'bloco_id' INTEGER ," +
                    "'ordem' INTEGER ," +
                    "'variavel' varchar(100))";

               // sql = 'DROP TABLE pergunta';

                db.transaction(function(tx){
                    tx.executeSql(sql,null,
                        function(){
                            console.log('Create table pergunta success');
                        },
                        function(tx, error){
                            console.log('Create table error: ' + error.message);
                        });
                });
            },

            createTableOpcao: function(db){

                var sql = "CREATE TABLE IF NOT EXISTS 'opcao' (" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    "'opcao' varchar(100) ," +
                    "'texto' INTEGER," +
                    "'pergunta_id' INTEGER ," +
                    "'valor' varchar(100))";


                db.transaction(function(tx){
                    tx.executeSql(sql,null,
                        function(){
                            console.log('Create table opcao success');
                        },
                        function(tx, error){
                            console.log('Create table error: ' + error.message);
                        });
                });
            },

            createTableLogica: function(db){

                var sql = "CREATE TABLE IF NOT EXISTS 'logica' (" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    "'opcao_id' INTEGER ," +
                    "'pergunta_id' INTEGER ," +
                    "'bloco_id' INTEGER ," +
                    "'tipo' varchar(100))";


                db.transaction(function(tx){
                    tx.executeSql(sql,null,
                        function(){
                            console.log('Create table logica success');
                        },
                        function(tx, error){
                            console.log('Create table error: ' + error.message);
                        });
                });
            },

            createTableEntrevista: function(db){

                var sql = "CREATE TABLE IF NOT EXISTS 'entrevista' (" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    "'questionario_id' INTEGER ," +
                    "'entrevistado__id' INTEGER ," +
                    "'dt_entrevista' DATE ," +
                    "'inicio' DATE TIME ,"+
                    "'termino' DATE TIME ,"+
                    "'atuacao_id' INTEGER,"+
                    "'sync' varchar(1) ,"+
                    "'usuario_id' INTEGER, "+
                    "'nome' varchar(45), "+
                    "'idade' varchar(45), "+
                    "'telefone' varchar(45), "+
                    "'sexo' varchar(45), "+
                    "'latlong' varchar(45))";

              // sql = 'DROP TABLE "entrevista"';

                db.transaction(function(tx){
                    tx.executeSql(sql,null,
                        function(){
                            console.log('Create table entrevista success');
                        },
                        function(tx, error){
                            console.log('Create table error: ' + error.message);
                        });
                });
            },

            createTableResposta: function(db){

                var sql = "CREATE TABLE IF NOT EXISTS 'resposta' (" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    "'opcao_id' INTEGER ," +
                    "'pergunta_id' INTEGER ," +
                    "'entrevista_id' INTEGER ," +
                    "'ultima_pergunta' INTEGER ," +
                    "'resposta' varchar(255),"+
                    "'prioridade' INTEGER,"+
                    "'sync' INTEGER)";

           //      sql = "DROP TABLE 'resposta'";
                db.transaction(function(tx){
                    tx.executeSql(sql,null,
                        function(){
                            console.log('Create table resposta success');
                        },
                        function(tx, error){
                            console.log('Create table error: ' + error.message);
                        });
                });
            },

            createTableHistorico: function(db){

                var sql = "CREATE TABLE IF NOT EXISTS 'historico' (" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    "'tipo' varchar(2) ," +
                    "'nome' varchar(100) ," +
                    "'avatar' LONGTEXT ," +
                    "'sync' varchar(2) ," +
                    "'dt_criacao' DATE TIME ," +
                    "'dt_lembrete' DATE ," +
                    "'comentario' LONGTEXT ," +
                    "'entrevistado_id' INTEGER,"+
                    "'usuario_id' varchar(1000))";

                // sql = "DROP TABLE 'historico'";

                db.transaction(function(tx){
                    tx.executeSql(sql,null,
                        function(){
                            console.log('Create table historico success');
                        },
                        function(tx, error){
                            console.log('Create table error: ' + error.message);
                        });
                });
            },

            createTableCota: function(db){

                var sql = "CREATE TABLE IF NOT EXISTS 'cota' (" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    "'nome' varchar(50),"+
                    "'pergunta_id' INTEGER)";

             //  sql = "DROP TABLE 'cota'";

                db.transaction(function(tx){
                    tx.executeSql(sql,null,
                        function(){
                            console.log('Create table cota success');
                        },
                        function(tx, error){
                            console.log('Create table error: ' + error.message);
                        });
                });
            },

            createTableFaixa: function(db){

                var sql = "CREATE TABLE IF NOT EXISTS 'faixa' (" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    "'faixa' varchar(50),"+
                    "'minimo' INTEGER,"+
                    "'maximo' INTEGER,"+
                    "'cotas_id' INTEGER,"+
                    "'opcao_id' INTEGER)";

             //  sql = "DROP TABLE 'faixa'";

                db.transaction(function(tx){
                    tx.executeSql(sql,null,
                        function(){
                            console.log('Create table faixa success');
                        },
                        function(tx, error){
                            console.log('Create table error: ' + error.message);
                        });
                });
            },



            createTableFrequencia: function(db){

                var sql = "CREATE TABLE IF NOT EXISTS 'frequencia' (" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    "'value' INTEGER," +
                    "'municipio_id' INTEGER," +
                    "'faixa_id' INTEGER)";

               //  sql = "DROP TABLE 'frequencia'";

                db.transaction(function(tx){
                    tx.executeSql(sql,null,
                        function(){
                            console.log('Create table  frequencia success');
                        },
                        function(tx, error){
                            console.log('Create table error: ' + error.message);
                        });
                });
            }

        }
    }
    angular.module('exercito.data').service('data.Tabelas', [Tabelas]);
}())