(function()
{
    var DB = function(Tabelas)
    {
            var size = 10 * 1024 * 1024,
            db = window.openDatabase('exercito','0.1','Mqmobile', size);

            return {
                getDb : function(){
                    return db;
                },
                /**
                * Abre o banco de dados.
                */
                open : function(){
                    Tabelas.createTables(db);
                },

                /**
                 * Efetua uma consulta no banco
                 */
                query : function(sql, callback){
                    var response = [];

                    db.transaction(
                        function (tx) {

                            tx.executeSql(sql, [], function (tx, results) {
                                for (i = 0; i < results.rows.length; i++) {
                                    response.push(results.rows.item(i));
                                }
                                callback(response);
                            });
                        },
                        function (error) {
                            //DEBUG.log("Transaction Error: " + error.message);
                            //DEBUG.log(sql);
                            callback(response);
                        }
                    );
                },

                /**
                 * Localiza um registro em uma determinada tabela pelos seus argumanetos passados como parametro..
                 */
                findBy : function(tabela, data,callback){
                    var response = [],aux=0;
                    db.transaction(
                        function (tx) {

                            var sql = "SELECT * FROM "+tabela;

                            Object.keys(data).forEach(function(key) {
                                if(aux == 0){
                                    sql +=" WHERE "+key+"= "+"'"+data[key]+"'";
                                }else{
                                    sql +=" AND "+key+"= "+"'"+data[key]+"'";
                                }
                                aux++;
                            });

                            tx.executeSql(sql, [], function (tx, results) {
                                for (i = 0; i < results.rows.length; i++) {
                                    response[i]=(results.rows.item(i));
                                }
                                callback(response);
                            });
                        },
                        function (error) {
                            console.log("Transaction Error: " + error.message);
                            callback(response);
                        }
                    );

                },

                findById : function (id,tabela,callback) {
                    var response = [];
                    db.transaction(
                        function (tx) {

                            var sql = "SELECT *" +
                                "FROM "+tabela+" WHERE id=:id";

                            tx.executeSql(sql, [id], function (tx, results) {
                                for (i = 0; i < results.rows.length; i++) {
                                    response[i]=(results.rows.item(i));
                                }

                                callback(response);
                            });
                        },
                        function (error) {
                            console.log("Transaction Error: " + error.message);
                            callback(response);
                        }
                    );
                },

                findOneBy : function (tabela, data,callback) {
                    this.findBy(tabela, data, function(results){
                        callback(results[0]);
                    });


                },

                save : function (tabela, data,callback) {

                    var into = [],
                        places = [],
                        values = [];

                    db.transaction(
                        function (tx) {

                            var sql = "INSERT OR REPLACE INTO "+tabela;

                            for (prop in data) {
                                into.push(prop);
                                places.push('?');
                                values.push(data[prop]);
                            }

                            sql += ' ('+into.join(', ')+') VALUES (' + places.join(', ') + ')';
                            console.log(sql);
                            tx.executeSql(sql, values,
                                function () {
                                    console.log('Save success: ' + tabela);
                                    callback(true);
                                },
                                function (tx, error) {
                                    console.log('Save error: ' + tabela + ' - Erro: ' + error.message);
                                    callback(false);
                                }
                            );

                        }
                    )
                },

                savePersistent : function (tabela, data,callback, tx) {

                    var into = [],
                        places = [],
                        values = [];

                    var sql = "INSERT OR REPLACE INTO "+tabela;

                    for (prop in data) {
                        into.push(prop);
                        places.push('?');
                        values.push(data[prop]);
                    }

                    sql += ' ('+into.join(', ')+') VALUES (' + places.join(', ') + ')';

                    tx.executeSql(sql, values,
                        function () {
//                                console.log('Save success');
                            callback();
                        },
                        function (tx, error) {
                            console.log('Save error: ' + error.message);
                            //callback();
                        }
                    );

                },


                remove : function (tabela, id, callback) {
                    db.transaction(
                        function (tx) {

                            var sql = "DELETE " +
                                "FROM "+tabela+" WHERE id= " + id;

                            console.log('Remove parametro :::::');
                            tx.executeSql(sql, [], function (tx, results) {
                                callback();
                            }, function(tx, error){
                                DEBUG.log('Remove error: ' + error.message);
                            });
                        }
                    )

                },

                removeTable : function(tabela){
                    db.transaction(
                        function (tx) {

                            var sql = "DELETE " +
                                "FROM "+tabela;

                            console.log('Remove Tabela :::::');
                            tx.executeSql(sql, [], function (tx, results) {

                            }, function(tx, error){
                                DEBUG.log('Remove error: ' + error.message);
                            });
                        }
                    )
                }

            }
    }
    angular.module('exercito.data').service('data.DB', ['data.Tabelas',DB]);
}())