(function()
{
    var SplashCtrl = function($scope,$http,$state, Parametro)
    {
        Parametro.get('pesquisador', function(pesquisador){
            if (pesquisador){
                $state.go('dashboard.resumo');
            } else {
                $state.go('login');
            }
        });

    };

    angular.module('exercito.init').controller('init.SplashCtrl',['$scope','$http','$state','common.Parametro', SplashCtrl]);

}());
