(function()
{
    var Sync = function(DB,$http,Parametro)
    {

     // var url = 'http://localhost/marketquery/web/api2';
        var url = 'http://mkpesquisa.marketquery.com.br/api2';

        var db = DB.getDb();
        var pesquisador_id, municipio_id;
        var totalDownload = 8;
        Parametro.get('pesquisador',function(pesquisador){pesquisador_id = pesquisador;});
        Parametro.get('municipio',function(municipio){municipio_id = municipio;});

        return{

            sync: function(callback){
                totalDownload = 8;
                this.updateEntrevista(callback, function(res){
                    return callback();
                });
            },

            download : function(callback){
                this.downloadEntrevista(callback);
                this.downloadBlocos(callback);
                this.downloadPerguntas(callback);
                this.downloadOpcoes(callback);
                this.downloadLogica(callback);
                this.downloadCota(callback);
                this.downloadFaixa(callback);
                this.downloadFrequencia(callback);
                this.downloadResposta(callback);

            },


            downloadEntrevista : function(callback){

                DB.removeTable('entrevista');

                $http.post(url+'/listEntrevista',{}).success(function(res){
                    db.transaction(function(tx){
                            var i = 0;
                            var len =  res.entrevista.length;
                            for(; i < len; i++){
                                DB.savePersistent('entrevista', res.entrevista[i],function(){},tx)
                            }
                            console.log(totalDownload);

                            if (totalDownload == 0){
                                callback();
                            }else{
                                totalDownload--;
                            }

                        },
                        function(error){
                            console.log("Transaction Error :"+ error.message);
                        });
                });
            },

            downloadBlocos : function(callback){

                DB.removeTable('bloco');

                $http.post(url+'/listBlocos',{}).success(function(res){
                    db.transaction(function(tx){
                            var i = 0;
                            var len =  res.blocos.length;
                            for(; i < len; i++){
                                DB.savePersistent('bloco', res.blocos[i],function(){},tx)
                            }
                            console.log(totalDownload);

                            if (totalDownload == 0){
                                callback();
                            }else{
                                totalDownload--;
                            }

                        },
                        function(error){
                            console.log("Transaction Error :"+ error.message);
                        });
                });
            },

            downloadPerguntas : function(callback){
                DB.removeTable('pergunta');

                $http.post(url+'/listPerguntas',{}).success(function(res){
                    db.transaction(function(tx){
                            var i = 0;
                            var len =  res.perguntas.length;
                            for(; i < len; i++){
                                DB.savePersistent('pergunta',res.perguntas[i],function(){},tx)
                            }
                            console.log(totalDownload);

                            if (totalDownload == 0){
                                callback();
                            }else{
                                totalDownload--;
                            }

                        },
                        function(error){
                            console.log("Transaction Error :"+ error.message);
                        });
                });
            },

            downloadOpcoes : function(callback){
                DB.removeTable('opcao');

                $http.post(url+'/listOpcoes',{}).success(function(res){
                    db.transaction(function(tx){
                            var i = 0;
                            var len =  res.opcao.length;
                            for(; i < len; i++){
                                DB.savePersistent('opcao',res.opcao[i],function(){},tx)
                            }
                            console.log(totalDownload);

                            if (totalDownload == 0){
                                callback();
                            }else{
                                totalDownload--;
                            }

                        },
                        function(error){
                            console.log("Transaction Error :"+ error.message);
                        });
                });
            },

            downloadLogica : function(callback){
                DB.removeTable('logica');

                $http.post(url+'/listLogicas',{}).success(function(res){
                    db.transaction(function(tx){
                            var i = 0;
                            var len =  res.logica.length;
                            for(; i < len; i++){
                                DB.savePersistent('logica',res.logica[i],function(){},tx)
                            }
                            console.log(totalDownload);
                            if (totalDownload == 0){
                                callback();
                            }else{
                                totalDownload--;
                            }

                        },
                        function(error){
                            console.log("Transaction Error :"+ error.message);
                        });
                });
            },

            downloadCota : function(callback){
                var id = Math.floor(pesquisador_id);
                DB.removeTable('cota');

                $http.post(url+'/listCotas',{pesquisador_id: id}).success(function(res){
                    db.transaction(function(tx){

                            var i = 0;
                            var len =  res.cota.length;
                            for(; i < len; i++){
                                DB.savePersistent('cota',res.cota[i],function(){},tx)
                            }
                            console.log(totalDownload);

                            if (totalDownload == 0){
                                callback();
                            }else{
                                totalDownload--;
                            }

                        },
                        function(error){
                            console.log("Transaction Error :"+ error.message);
                        });
                });
            },

            downloadFaixa : function(callback){

                DB.removeTable('faixa');
                var id = Math.floor(pesquisador_id);

                $http.post(url+'/listFaixa',{pesquisador_id: id}).success(function(res){
                    db.transaction(function(tx){
                            var i = 0;
                            if (res.faixa != null) {
                                var len =  res.faixa.length;
                                for(; i < len; i++){
                                    DB.savePersistent('faixa',res.faixa[i],function(){},tx)
                                }
                                console.log(totalDownload);

                                if (totalDownload == 0){
                                    callback();
                                }else{
                                    totalDownload--;
                                }
                            } else {
                                console.log(totalDownload);

                                if (totalDownload == 0){
                                    callback();
                                }else{
                                    totalDownload--;
                                }
                            }

                        },
                        function(error){
                            console.log("Transaction Error :"+ error.message);
                        });
                });
            },

            downloadResposta : function(callback){

                DB.removeTable('resposta');
                var id = Math.floor(pesquisador_id);

                $http.post(url+'/listResposta',{municipio_id: municipio_id}).success(function(res){
                    db.transaction(function(tx){
                            var i = 0;
                            if (res.resposta != null) {
                                var len =  res.resposta.length;
                                for(; i < len; i++){
                                    DB.savePersistent('resposta',res.resposta[i],function(){},tx)
                                }
                                console.log(totalDownload);

                                if (totalDownload == 0){
                                    callback();
                                }else{
                                    totalDownload--;
                                }
                            } else {
                                console.log(totalDownload);

                                if (totalDownload == 0){
                                    callback();
                                }else{
                                    totalDownload--;
                                }
                            }

                        },
                        function(error){
                            console.log("Transaction Error :"+ error.message);
                        });
                });
            },

            downloadFrequencia : function(callback){

                DB.removeTable('frequencia');

                $http.post(url+'/listFrequencia',{pesquisador_id: pesquisador_id}).success(function(res){
                    db.transaction(function(tx){
                            var i = 0;
                            if (res.frequencia != null) {
                                var len =  res.frequencia.length;
                                for(; i < len; i++){
                                    DB.savePersistent('frequencia',res.frequencia[i],function(){},tx)
                                }
                                console.log(totalDownload);

                                if (totalDownload == 0){
                                    callback();
                                }else{
                                    totalDownload--;
                                }
                            } else {
                                console.log(totalDownload);

                                if (totalDownload == 0){
                                    callback();
                                }else{
                                    totalDownload--;
                                }
                            }

                        },
                        function(error){
                            console.log("Transaction Error :"+ error.message);
                        });
                });
            },

            updateEntrevista : function(callback){
                var sqlEntrevista = "SELECT * FROM 'entrevista' e WHERE e.sync IS NULL";
                var sqlRespostas = "SELECT * FROM 'resposta' r WHERE r.sync IS NULL";
                var me = this;

                DB.query(sqlEntrevista,function(entrevista){
                    DB.query(sqlRespostas,function(resposta){

                         $http.post(url+'/updateDados', {entrevista:entrevista,resposta:resposta}).success(function(res){
                            if (res.success == true) {
                                DB.removeTable('entrevista');
                                DB.removeTable('resposta');
                                me.download(callback);
                            }
                         });
                    });
                });
            }

        }
    };

    angular.module('exercito.commom').service('common.Sync', ['data.DB','$http','common.Parametro', Sync]);
}())