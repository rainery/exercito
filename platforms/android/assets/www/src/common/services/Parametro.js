(function()
{
    var Parametro = function(DB)
    {
        return {
            /**
             * Seta o valor de um parâmetro.
             *
             * @param chave
             * @param valor
             * @param callback
             */
            set : function(chave, valor, callback){
                DB.save('parametro',{
                    'chave': chave,
                    'valor': valor
                }, callback);
            },

            /**
             * Recupera o valor de uma chave na tabela de parâmetros.
             *
             * @param chave
             * @param callback
             */
            get : function(chave, callback){
                DB.findOneBy('parametro', {'chave': chave}, function(parametro){
                    if (parametro){
                        callback(parametro.valor);
                    } else {
                        callback(false);
                    }
                });
            },

            /**
             * Remove uma chave da tabela de parâmetros.
             *
             * @param chave
             * @param callback
             */
            remove : function(chave, callback){
                DB.findOneBy('parametro',{
                    chave: chave
                }, function(record){
                    if (record){
                        DB.remove('parametro',record.id, callback);
                    }
                })
            }
        }
    }

    angular.module('exercito.commom').service('common.Parametro', ['data.DB', Parametro]);
}())