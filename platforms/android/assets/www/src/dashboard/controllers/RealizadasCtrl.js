(function()
{
    var RealizadasCtrl = function($scope, $ionicModal,DB, Entrevistado,$state, Dados, Sync, $ionicLoading, Alert, Parametro)
    {

        Entrevistado.setStatusEntrevista(1);
        Entrevistado.setEntrevista(null);
        Entrevistado.setUltimaPergunta();
        Entrevistado.setEdit(1);


        var sqlEntreivsta = "SELECT * FROM 'entrevista'";
        DB.query(sqlEntreivsta,function(entrevistas){
            var i = entrevistas.length;
            if(i == 0){
                $scope.$apply(function () {
                    $scope.entrevistas = [];
                });
            }
            i--;
            angular.forEach(entrevistas, function(entrevista){
                var sqlResposta = "SELECT * FROM 'resposta' r WHERE r.entrevista_id = "+entrevista.id;
                DB.query(sqlResposta,function(repostas){
                    entrevista.respostas = {};
                    entrevista.respostas = repostas;
                    console.log(i);
                    if(i == 0){
                        $scope.$apply(function () {
                            $scope.entrevistas = entrevistas;
                        });
                    } else {
                        i--;
                    }
                });
            });
        });


        $scope.edit = function(entrevista){

            Entrevistado.setStatusEntrevista(1);
            Entrevistado.setEntrevista(entrevista);
            $state.go("entrevista-list-blocos");
        };


        $scope.sync = function(){
            $ionicLoading.show({
                template: 'Sincronizando...'
            });

            Sync.sync(function(res){
                Alert.alert('Registros sincronizados');
               $scope.listMore(0,null);
            });
        }

    };
    angular.module('exercito.dashboard').controller('dashboard.RealizadasCtrl',['$scope','$ionicModal','data.DB','dashboard.Entrevistado','$state','dashboard.Dados','common.Sync','$ionicLoading','common.Alert','common.Parametro', RealizadasCtrl]);
}());
