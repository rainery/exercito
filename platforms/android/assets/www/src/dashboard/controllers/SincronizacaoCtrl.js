(function()
{
    var SincronizacaoCtrl = function($scope, Sync, $state)
    {

        Sync.sync(function(res){
            $state.go("dashboard.resumo");
        });

    };
    angular.module('exercito.dashboard').controller('dashboard.SincronizacaoCtrl',['$scope','common.Sync','$state', SincronizacaoCtrl]);
}());
