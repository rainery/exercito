(function()
{
    var getView = function(view){
        return 'src/dashboard/views/' + view + '.html';
    }

    var dashboardModule = angular.module('exercito.dashboard',[]);

    dashboardModule.config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider){


        $stateProvider.state('dashboard', {
                url: "/dashboard",
                abstract: true,
                templateUrl: getView('menu'),
                controller: 'dashboard.MenuCtrl'
            })

            .state('dashboard.realizadas', {
                url: '/realizadas',
                cache:false,
                views: {
                    'menuContent': {
                        templateUrl: getView('realizadas'),
                        controller: 'dashboard.RealizadasCtrl'
                    }
                }

            })

            .state('sincronizacao', {
                url: '/sincronizacao',
                cache: false,
                templateUrl: getView('sincronizacao'),
                controller: 'dashboard.SincronizacaoCtrl'
            })

            .state('dashboard.resumo', {
                url: '/resumo',
                cache:false,
                views: {
                    'menuContent': {
                        templateUrl: getView('resumo'),
                        controller: 'dashboard.ResumoCtrl'
                    }
                }

            });

    }])
}());