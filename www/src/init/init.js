(function()
{
    var getView = function(view){
        return 'src/init/views/' + view + '.html';
    }

    var initModule = angular.module('exercito.init',['exercito.data']);

    initModule.config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise('/');

        $stateProvider.state('splash', {
            url: '/',
            cache: false,
            templateUrl: getView('splash'),
            controller: 'init.SplashCtrl'
        })
       .state('download', {
            url: '/download',
            cache: false,
            templateUrl: getView('download'),
            controller: 'init.DownloadCtrl'
        })
        .state('login',{
            url: '/login',
            cache: false,
            templateUrl: getView('login'),
            controller: 'init.InitCtrl'
        })
        ;
    }])
    .run(['data.DB', function(DB){
        DB.open();
    }])

}());