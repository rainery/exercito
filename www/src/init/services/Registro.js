(function()
{
    /**
     * Registro.
     *
     * @constructor
     */
    var Registro = function(Parametro)
    {
        return {

            /**
             * Salva a contaId do dispositivo
             *
             * @param id
             * @return Store
             */
            setContaId : function(id){
                Parametro.set('conta',id, function(){});
            },

            /**
             * Retorna a contaId do dispositivo.
             *
             * @return conta_id
             */
            getContaId : function(callback){
                console.log(conta_id);
                return conta_id;
            }


        }
    };

    // retorna o array construtor do serviço
    angular.module('exercito.init').service('init.Registro', ['common.Parametro', Registro]);

}());