(function()
{
    var InitCtrl = function($scope,$http, $state,Parametro, Alert, $cordovaDevice, $ionicLoading)
    {


       var url = 'http://mkpesquisa.marketquery.com.br/api2';
       // var url = 'http://localhost/marketquery/web/api2';

        $scope.usuario = {};
        $scope.usuario.municipio_id = "";
        $scope.usuario.regiao_id = "";

        $scope.logar = function(usuario){

            $ionicLoading.show({
                template: 'Autenticando...'
            });

            $http.post(url+'/login', usuario).success(function(res){
                $ionicLoading.hide();
                if (res.success == true) {
                    Parametro.set('municipio',res.municipio_id, function(){});
                    Parametro.set('pesquisador',res.usuario_id, function(){});
                    Parametro.set('avatar', res.avatar, function(){});
                    Parametro.set('nome', res.nome, function(){});
                    $state.go('download');
                } else {
                    Alert.alert('Esse usuário não existe!')
                }
            });
        };


        $http.post(url+'/listRegiao').success(function(res){
            $scope.regioes = res.regiao;
        });

        $scope.$watch('usuario.regiao_id', function(novo,antigo){
            console.log(novo);
            if(novo != null && novo != undefined && novo != ""){
                $http.post(url+'/listMunicipio', {regiao_id:novo}).success(function(res){
                    $scope.municipios = res.municipio;
                });
            }
        });

    };
    angular.module('exercito.init').controller('init.InitCtrl',['$scope','$http','$state','common.Parametro','common.Alert','$cordovaDevice','$ionicLoading', InitCtrl]);
}());
