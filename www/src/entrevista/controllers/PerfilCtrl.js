(function()
{
    var PerfilCtrl = function($scope,$state, $stateParams, DB, Entrevistado, Parametro)
    {
        $scope.entrevistado = {};
        $scope.entrevistado = Entrevistado.getEntrevistado();

        Parametro.get("view",function(res){
            $scope.view = res;
        });

        $scope.goEntrevista = function(atuacao){
            //Seta a atuação atual a respoder o questionario com intuito de salvar a entrevista com uma atuação_id

            Entrevistado.setAtuacao(atuacao);
            var sqlQuestionario = "SELECT * FROM 'questionario' q WHERE q.perfil = '"+$scope.entrevistado.PERFIL+"'";
            DB.query(sqlQuestionario,function(questionario){

               if(questionario.length > 0){
                     var sqlBloco = "SELECT * FROM 'bloco' b WHERE b.questionario_id = '"+questionario[0].id+"' ORDER BY b.ordem";
                     Entrevistado.setQuestionario(questionario[0].id);
                     DB.query(sqlBloco,function(blocos){
                    var totalBlocos = blocos.length, b = 0;
                    $scope.blocos = blocos;

                    angular.forEach($scope.blocos, function(bloco){
                        b++;
                        var sqlPergunta = "SELECT * FROM 'pergunta' p WHERE p.bloco_id = '"+bloco.id+"' ORDER BY p.ordem";
                        DB.query(sqlPergunta,function(perguntas){
                            bloco.perguntas = perguntas;
                            var totalPerguntas = perguntas.length, p = 0;

                            // Foreach no objeto de perguntas para cada pergunta dentro dele pegar as opçoes
                            angular.forEach(perguntas, function(pergunta){
                                var sqlOpcao = "SELECT o.*, l.pergunta_id as logica_pergunta_id, l.bloco_id FROM 'opcao' o LEFT JOIN 'logica' l ON l.opcao_id = o.id WHERE o.pergunta_id = '"+pergunta.id+"'";
                                DB.query(sqlOpcao,function(opcoes){
                                    p++;
                                    pergunta.opcoes = opcoes;

                                    if (p == totalPerguntas && b == totalBlocos) {

                                         Entrevistado.setBlocoEntrevistado(blocos);
                                         if (atuacao.inicio == null) {
                                             $state.go('entrevista-pergunta',{perguntaId: $scope.blocos[0].perguntas[0].id})
                                         } else if (atuacao.inicio != null && atuacao.termino == null) {
                                             Parametro.get(atuacao.id,function (res) {
                                                 $state.go('entrevista-pergunta',{perguntaId: res})
                                             });
                                         } else {
                                             if($scope.blocos.lenght > 1){
                                                 $state.go('entrevista-list-blocos',{});
                                             } else {
                                                 Entrevistado.setPerguntasEntrevistado($scope.blocos[0].perguntas);
                                                 $state.go('entrevista-list-perguntas',{});
                                             }
                                         }
                                    }
                                });
                            });
                        });
                    });

                });
               } else {
                 alert("Esse perfil não tem questionário!");
               }
            });
        }

        $scope.back = function(){
            if ($scope.view == 'entrevistado') {
                $state.go('entrevistado.entrevistados');
            } else {
                $state.go('entrevistado.agenda');
            }
        }

    };
    angular.module('exercito.entrevista').controller('entrevista.PerfilCtrl',['$scope','$state','$stateParams','data.DB','entrevista.Entrevistado','common.Parametro', PerfilCtrl]);
}());
