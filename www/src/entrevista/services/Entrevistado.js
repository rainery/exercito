(function()
{
    var Entrevistado = function()
    {
            var perguntas = {};
            var blocos = {};
            var entrevista = null;
            var questionario, status, index , retorno, prioridade, ultima_pergunta, edit;


        return {

                /*
                Seta a ultima pergunta
                 */
                setEdit: function(data){
                    edit = data;
                },
                /*
                Seta a ultima pergunta
                 */
                setUltimaPergunta: function(data){
                    ultima_pergunta = data;
                },
                /*
                Seta o retorno
                 */
                setPrioridade: function(data){
                    prioridade = data;
                },
                 /*
                Seta o retorno
                 */
                setRetorno: function(data){
                    retorno = data;
                },
                /*
                Retorne a variável ao controller
                 */
                getEntrevista: function(){
                    return entrevista;
                },

                /*
                Seta a variável dashboard no service
                 */
                setEntrevista: function(data){
                    entrevista = data;
                },

                /*
                Seta a variável questionario no service
                 */
                setQuestionario: function(id){
                    questionario = id;
                },

                /*
                 Seta a variável index para voltar do retorno do banco
                 */
                setIndex: function(i){
                    index = i;
                },


                /*
                   Seta as perguntas para o dashboard
                 */
                setPerguntasEntrevistado : function(data){
                    perguntas = data;
                },

                /*
                 Seta os blocos
                 */
                setBlocoEntrevistado : function(data){
                    blocos = data;
                },

            /*
             Seta o status da entrevista
             */
                setStatusEntrevista : function(data){
                    status = data;
                },

                /*
                 Retorna o status da entrevista
                 */
                getStatusEntrevista : function(){
                    return status;
                },

                 /*
                 Retorna o status da entrevista
                 */
                getIndex : function(){
                    return index;
                },

                /*
                  Retorna as perguntas ao dashboard
                 */
                getPerguntasEntrevistado : function(){
                    return perguntas;
                },

            /*
             Retorna os blocos
             */
            getBlocoEntrevistado : function(){
                return blocos;
            },

            /*
             Retorna o retorno
             */
            getRetorno : function(){
                return retorno;
            },

            /*
             Retorna a prioridade
             */
            getPrioridade : function(callbak){
                return callbak(prioridade);
            },
             /*
             Retorna a ultima pergunta
             */
            getUltimaPergunta : function(){
                return ultima_pergunta;
            },


            /*
             Retorna o questionario Id
             */
                getQuestionario: function(){
                    return questionario;
                },

            /*
             Retorna o questionario Id
             */
                getEdit: function(){
                    return edit;
                }


            }
    }
    angular.module('exercito.data').service('dashboard.Entrevistado', [Entrevistado]);
}())