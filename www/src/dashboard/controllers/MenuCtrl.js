(function()
{
    var Menu = function($scope, $state, Alert, Parametro, Sync, DB, $ionicPopup, $ionicLoading, Dados,Entrevistado)
    {

        $scope.usuario = {};
        Parametro.get('nome', function(nome){
            $scope.usuario.nome = nome;
            console.log(nome);
            $scope.$apply();
        });
        $scope.usuario.projeto = "Pesquisas portuarias";
        $scope.usuario.empresa = "ANTAQ";
        Parametro.get('avatar',function(avatar){$scope.usuario.avatar = avatar;
            $scope.$apply();

        });


        $scope.logout = function(){
            var msg = 'Deseja sair da aplicação?';

            Alert.confirm(msg, function(btn){

                if (btn == 1) {
                    Parametro.remove('nome',function(){});
                    Parametro.remove('avatar',function(){});
                    Parametro.remove('municipio',function(){});
                    Parametro.remove('pesquisador', function(){
                        $state.go('login');
                        $scope.$apply();
                    });

                }
            });
        };

        $scope.showAlert = function() {
            var alertPopup = $ionicPopup.alert({
                title: 'Erro ao sincronizar',
                template: 'Existem entrevistas abertas, favor enceralas antes da sincronização!'
            });
        };

        $scope.sync = function(){
          $state.go("sincronizacao");
        };


        $scope.newEntrevista =  function(){

                        var sqlBloco = "SELECT * FROM 'bloco' b ORDER BY b.ordem";
                        DB.query(sqlBloco,function(blocos){

                            var totalBlocos = blocos.length, b = 0;
                            $scope.blocos = blocos;

                            angular.forEach($scope.blocos, function(bloco){
                                b++;
                                var sqlPergunta = "SELECT * FROM 'pergunta' p WHERE p.bloco_id = '"+bloco.id+"' ORDER BY p.ordem";
                                DB.query(sqlPergunta,function(perguntas){

                                    bloco.perguntas = perguntas;
                                    var totalPerguntas = perguntas.length, p = 0;

                                    // Foreach no objeto de perguntas para cada pergunta dentro dele pegar as opçoes
                                    angular.forEach(perguntas, function(pergunta){
                                        var sqlOpcao = "SELECT o.*, l.pergunta_id as logica_pergunta_id, l.bloco_id FROM 'opcao' o LEFT JOIN 'logica' l ON l.opcao_id = o.id WHERE o.pergunta_id = '"+pergunta.id+"'";
                                        DB.query(sqlOpcao,function(opcoes){
                                            p++;
                                            pergunta.opcoes = opcoes;

                                            if (p == totalPerguntas && b == totalBlocos) {
                                                 console.log("!AASDF");
                                                Entrevistado.setBlocoEntrevistado(blocos);
                                                $state.go('entrevista-pergunta',{perguntaId: $scope.blocos[0].perguntas[0].id})

                                            }
                                        });
                                    });
                                });
                            });

                        });

        };

    };
    angular.module('exercito.dashboard').controller('dashboard.MenuCtrl',['$scope','$state','common.Alert','common.Parametro','common.Sync','data.DB','$ionicPopup','$ionicLoading','dashboard.Dados','dashboard.Entrevistado', Menu]);
}());
