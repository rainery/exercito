(function()
{
    var ResumoCtrl = function($scope, $ionicModal,DB, Entrevistado,$state, Dados, Sync, $ionicLoading, Alert, Parametro)
    {
        $scope.Dados = Dados;
        $scope.pesquisa = '';
        $scope.scrollLoad = 1;
        var cotas;
        Entrevistado.setStatusEntrevista(0);
        Entrevistado.setEntrevista(null);
        Entrevistado.setUltimaPergunta();
        Entrevistado.setEdit(0);

        Parametro.get('municipio',function(municipio){
            var sqlCotas = 'SELECT c.* FROM cota c';
            DB.query(sqlCotas, function(cot){
                cotas = angular.copy(cot);
                angular.forEach(cotas, function(cota){
                    var sqlFaixas = 'SELECT f.* FROM faixa f WHERE f.cotas_id = '+cota.id;

                        DB.query(sqlFaixas, function(faixas){
                             cota.faixas = {};
                             cota.faixas = faixas;
                            angular.forEach(cota.faixas, function(faixa){
                                if (faixa.opcao_id != null) {
                                    var sqlRespostas = 'SELECT COUNT(*) as total FROM resposta r WHERE r.pergunta_id = '+cota.pergunta_id+' AND r.opcao_id = '+faixa.opcao_id;
                                } else {
                                    var sqlRespostas = 'SELECT COUNT(*) as total FROM resposta r WHERE r.pergunta_id = '+cota.pergunta_id+' AND r.resposta >= '+faixa.minimo+' AND r.resposta <= '+faixa.maximo;
                                }
                                DB.query(sqlRespostas, function(respotas){
                                    var sqlFrequencia = 'SELECT f.* FROM frequencia f WHERE f.faixa_id = '+faixa.id+
                                        ' AND f.municipio_id = '+municipio+'';
                                    DB.query(sqlFrequencia, function(frequencia){
                                        faixa.frequencia = frequencia[0];
                                        faixa.entrevistas = respotas[0].total;
                                        $scope.$apply();
                                    });
                                });
                        });
                    });

                });
                $scope.cotas = cotas;
                $scope.$apply();

            });
        });


    };
    angular.module('exercito.dashboard').controller('dashboard.ResumoCtrl',['$scope','$ionicModal','data.DB','dashboard.Entrevistado','$state','dashboard.Dados','common.Sync','$ionicLoading','common.Alert','common.Parametro', ResumoCtrl]);
}());
