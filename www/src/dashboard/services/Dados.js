(function()
{
    var Dados = function(DB)
    {
        return {
            agenda: [],
            entrevistados: [],

            /*
             Retorne a lista de agenda do pesquisador
            */
            listMore: function(number,query, callback) {

                var me = this;
                if (number == 0) {
                    me.agenda = [];
                }

                if (query != null && query != '' && query != undefined && query != 'null') {
                    var sql = "SELECT e.*, et.termino, et.inicio , et.sync, et.id as entrevista_id FROM 'dashboard' e" +
                        " JOIN 'agendamento' a ON e._id = a.entrevistado_id "+
                        " LEFT JOIN 'entrevista' et ON et.entrevistado__id = e._id"+
                        " WHERE e.id LIKE '"+ query +"%'";
                } else {
                    var sql = "SELECT e.*, et.termino, et.inicio , et.sync, et.id as entrevista_id FROM 'dashboard' e" +
                        " JOIN 'agendamento' a ON e._id = a.entrevistado_id "+
                        " LEFT JOIN 'entrevista' et ON et.entrevistado__id = e._id";
                }

                DB.query(sql, function(entrevistados){

                    var index = entrevistados.length,
                    i = 0;
                    if(entrevistados.length >= 1) {
                       angular.forEach(entrevistados, function(entrevistado){
                            var sqlResposta = "SELECT r.* FROM 'resposta' r WHERE r.entrevista_id = '"+ entrevistado.entrevista_id+"' AND r.sync IS NULL";
                            DB.query(sqlResposta, function(res){
                               if (res.length != 0) {
                                    entrevistados[i].sync = null;
                               }
                               if (i == index-1){
                                    me.agenda = entrevistados;
                                   callback();
                               }
                               i++;
                            });
                       });
                    } else {
                        console.log('AQUI');
                        callback();
                    }
                });
            },

            /*
             Retorne a lista de entrevistados
             */
            listMoreEntrevistados: function(number,query, callback) {

                var me = this;
                if (number == 0) {
                    me.entrevistados = [];
                }
                console.log(query);
                if (query != null && query != '' && query != undefined && query != 'null') {
                    var sql = "SELECT e.* FROM 'dashboard' e" +
                        " WHERE e.id LIKE '"+ query +"%'";
                } else {
                   var sql = "SELECT e.* FROM 'dashboard' e LIMIT "+number+",4";
                }
                console.log(sql);
                console.log("BUSCANDO OS ENTREVISTADOS");

                DB.query(sql, function(entrevistados){
                    console.log(entrevistados);
                    var index = entrevistados.length,
                        i = 0;
                    angular.forEach(entrevistados, function(entrevistado){
                        console.log('for each entrevistados');
                        var sqlAtuacao = "SELECT "+
                            " at.id,at.entrevistado__id,at.atuacao_id, a.atuacao, et.termino, et.sync, et.inicio, r.sync as resposta_sync, " +
                            " et.id as entrevista_id " +
                            " FROM 'atuacao' a"+
                            " JOIN 'area_atuacao' at ON at.atuacao_id = a.id"+
                            " LEFT JOIN 'entrevista' et ON et.atuacao_id = at.id AND et.entrevistado__id = at.entrevistado__id"+
                            " LEFT JOIN 'resposta' r ON r.entrevista_id = et.id"+
                            " WHERE at.entrevistado__id = '"+entrevistado._id+"' GROUP BY at.id";
                        var a = 0;
                        console.log('Buscando atuaçoes ...');
                        DB.query(sqlAtuacao, function(atuacoes){
                            console.log('Buscou atuaçoes');
                           entrevistado.atuacao = [];
                           var totalAtuacoes = atuacoes.length;
                            // console.log('atuacoes: '+totalAtuacoes);
                            console.log('-----');
                            if(totalAtuacoes >= 1){
                                console.log("NUMERO de ATUACOES MAIOR que UM");
                                angular.forEach(atuacoes, function(atuacao){
                                    console.log("ATUACOES");
                                    var sqlResposta = "SELECT r.* FROM 'resposta' r WHERE r.entrevista_id = '"+ atuacao.entrevista_id+"' AND r.sync IS NULL";
                                   console.log('antes de respostas');
                                    DB.query(sqlResposta, function(respostas){
                                        console.log('depois de respotas');
                                       a++;
                                      if(respostas.length >= 1) {
                                          atuacao.sync = null;
                                     }
                                        console.log('DENTRO DE RESPOSTAS');
                                       if (a == totalAtuacoes) {
                                           console.log('AQUI AS ATUAÇOES:'+atuacoes);
                                            entrevistado.atuacao = atuacoes;
                                            i++;
                                       }
                                        if (i == index) {
                                            console.log('Finalizou consulta!');
                                            console.log(entrevistados);
                                            me.entrevistados = me.entrevistados.concat(entrevistados);
                                            callback();
                                        }
                                  });
                                });
                            } else {
                               i++;
                               if (i == index){
                                 console.log('Finalizou consulta!');
                                    console.log(entrevistados);
                                    me.entrevistados = me.entrevistados.concat(entrevistados);
                                    callback();
                                }
                            }
                       });

                    });
                });
            }

        }

    }
    angular.module('exercito.dashboard').service('dashboard.Dados', ['data.DB', Dados]);
}())