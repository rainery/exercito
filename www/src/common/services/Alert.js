(function()
{
    var Alert = function($cordovaToast)
    {
        return {
            /**
             * Exibe uma mensagem de notificação.
             *
             * @param msg
             * @param position
             */
            alert : function(msg, position){
                if (!position){
                    position = 'top';
                }

                if (window.cordova){
                    $cordovaToast.show(msg, 'long', position);
                } else {
                    alert(msg);
                }
            },

            confirm : function(msg, callback){
                if (navigator.notification){
                    navigator.notification.confirm(msg, function(btn){
                        callback(btn);
                    });
                } else {
                    btn = window.confirm(msg);
                    callback(btn);
                }

            }
        }

    }

    angular.module('exercito.commom').service('common.Alert', ['$cordovaToast', Alert]);
}())